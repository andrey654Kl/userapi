<?php

use yii\db\Migration;

class m161115_053648_add_user_subscribe extends Migration
{
    public function up()
    {
        $this->createTable('user_subscription', [
            'userID'=>$this->primaryKey(),
            'endDate'=>$this->timestamp()
        ]);
        $this->addForeignKey('fk_user1', 'user_subscription', 'userID', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_user1', 'user_subscription');
        $this->dropTable('user_subscription');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
