<?php

use yii\db\Migration;

class m161115_053639_add_user extends Migration
{
    public function up()
    {
          $this->createTable('user', [
              'id'=>$this->primaryKey(),
              'login'=>$this->string(255)->notNull(),
              'password'=>$this->string(255)->notNull(),
              'email'=>$this->string(255)
          ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
