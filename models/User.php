<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $email
 *
 * @property UserSubscription $userSubscription
 */
class User extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['login'], 'required'],
            ['login', 'unique','targetClass'=>self::className(),'targetAttribute'=>'login','message'=>'Данное имя пользователя занято','on'=>'create'],
            ['password', 'required', 'on' => 'create'],
            [['login', 'password', 'email'], 'string', 'max' => 255],
            ['email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'login' => 'Имя Пользователя',
            'password' => 'Пароль',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSubscription() {
        return $this->hasOne(UserSubscription::className(), ['userID' => 'id']);
    }

    static function encryptPassword($password) {
        return md5($password);
    }

    public function fields() {
        return [
            'id',
            'login',
            'endDateSubscribe' => function () {
                if (isset($this->userSubscription)) {
                    return date('d-m-Y', strtotime($this->userSubscription->endDate));
                }
                return null;
            },
        ];
    }

}
