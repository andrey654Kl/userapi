<?php

namespace app\models\forms;

use app\models\User;
use app\models\UserSubscription;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserForm
 *
 * @author kas
 */
class UserForm extends User {

    //put your code here
    public $endDateSubscribe;
    private $_user;

    public function rules() {
        $data = parent::rules();
        $data[] = ['endDateSubscribe', 'safe'];
        return $data;
    }

    function __construct($config = array()) {
        parent::__construct();
        if (isset($config['user'])) {
            $this->_user = $config['user'];
            $this->attributes = $this->_user->attributes;
            if (isset($this->_user->userSubscription)) {
                $this->endDateSubscribe = date('d-m-Y', strtotime($this->_user->userSubscription->endDate));
            }
            unset($this->password);
        }
    }

    public function attributeLabels() {
        $attr = parent::attributeLabels();
        $attr['endDateSubscribe'] = 'Дата окончания подписки';
        return $attr;
    }

    public function save($runValidation = true, $attributeNames = null) {
        if (!$this->validate()) {
            return false;
        }
        $user = $this->_user ? : new User;
        if (!$this->password) {
            $this->password = $user->password;
        } else {
            $this->password = self::encryptPassword($this->password);
        }
        $user->attributes = $this->attributes;
        $user->save();
        if ($this->endDateSubscribe) {
            $sub = UserSubscription::findOne(['userID' => $user->id]);
            if (!$sub) {
                $sub = new UserSubscription();
                $sub->userID = $user->id;
            }
            $sub->endDate = date('Y-m-d 23:59:59', strtotime($this->endDateSubscribe));
            $sub->save();
        } else {
            UserSubscription::deleteAll(['userID' => $user->id]);
        }
        return true;
    }

}
