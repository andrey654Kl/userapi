<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\api\controllers;

use yii\rest\ActiveController;
use Yii;
/**
 * Description of UserController
 *
 * @author kas
 */
class UserController extends ActiveController {

    //put your code here
    public $modelClass = 'app\models\User';

    public function actions() {
        $actions = parent::actions();
        unset($actions['update']);
        return $actions;
    }

    function actionUpdate($id) {
        $user = \app\models\User::findOne($id);
        if (!$user) {
            throw new \yii\web\NotFoundHttpException("Object not found: $id");
        }
        $model = new \app\models\forms\UserForm(['user' => $user]);
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save() === false && !$model->hasErrors()) {
            throw new \yii\web\ServerErrorHttpException('Failed to update the object for unknown reason.');
        }
        $newUser = \app\models\User::findOne($id);
        return $newUser;
    }

}
