<?php
namespace app\modules\admin\models;
use app\models\User;
use yii\data\ActiveDataProvider;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserGrid
 *
 * @author kas
 */
class UserGrid extends User{
    //put your code here
    public $subscribeEndDate;
    
     function rules() {
        return [
            [['id', 'login', 'email', 'subscribeEndDate'], 'safe'],
        ];
    }
   


    public function search() {
        $query = self::find();
        $query->select([
            'u.id',
            'u.login',
            'u.email',
            'subscribeEndDate' => 'sub.endDate',
        ]);
        $query->from(['u' => self::tableName()]);
        $query->leftJoin(['sub' => \app\models\UserSubscription::tableName()], 'sub.userID = u.id');
        $query->indexBy = 'id';
        $query->filterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'u.login', $this->login]);
        $query->andFilterWhere(['like', 'u.email', $this->email]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['id','login','email','subscribeEndDate']]
        ]);
        return $dataProvider;
    }
}
