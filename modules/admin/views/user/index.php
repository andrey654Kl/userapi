<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'layout' => '{items}{summary}{pager}',
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['width' => '70px']
            ],
            'login',
            'email',
            [
                'attribute' => 'subscribeEndDate',
                'label' => 'Дата окончания подписки',
                'value' => function($row) {
                    if ($row->subscribeEndDate) {
                        return date('d-m-Y', strtotime($row->subscribeEndDate));
                    }
                    return null;
                },
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'user',
                'options' => ['width' => '70px']
            ]
        ],
    ]);
    ?>
</div>
